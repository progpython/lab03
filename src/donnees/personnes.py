# -*- coding: UTF-8 -*-
'''
Created on 28 janv. 2014
updated on 2014-10-03 for Python 3.4
@author: Johnny Tsheke
'''

pers1={"prenom":"Martin","nom":"Tremblay","codePays":"ca"}
pers2={"prenom":"François","nom":"De Gaule","codePays":"fr"}
pers3={"prenom":"Sylvie","nom":"Kadima","codePays":"cd"}
pers4={"prenom":"Li","nom":"Fu","codePays":"cn"}
pers5={"prenom":"Tony","nom":"Doyle","codePays":"au"}
