# -*- coding: UTF-8 -*-
'''
Created on 2014-01-28
updated on 2014-10-03 for Python 3.4

@author: Johnny Tsheke
'''
from donnees import personnes
from donnees import pays

cp=pays.codes
print("Informations sur les personnes")
print("---------------------------------")

pers=personnes.pers1
code=pers["codePays"]
residence=cp[code]
print("Nom: ",end=" ")
print(pers["nom"],pers["prenom"],sep=", ")
print("Pays de résidence: ",residence)
print("---------------------------------")
pers=personnes.pers2
code=pers["codePays"]
residence=cp[code]
print("Nom: ",end=" ")
print(pers["nom"],pers["prenom"],sep=", ")
print("Pays de résidence: ",residence)
print("---------------------------------")
pers=personnes.pers3
code=pers["codePays"]
residence=cp[code]
print("Nom: ",end=" ")
print(pers["nom"],pers["prenom"],sep=", ")
print("Pays de résidence: ",residence)
print("---------------------------------")
pers=personnes.pers4
code=pers["codePays"]
residence=cp[code]
print("Nom: ",end=" ")
print(pers["nom"],pers["prenom"],sep=", ")
print("Pays de résidence: ",residence)
print("---------------------------------")
pers=personnes.pers5
code=pers["codePays"]
residence=cp[code]
print("Nom: ",end=" ")
print(pers["nom"],pers["prenom"],sep=", ")
print("Pays de résidence: ",residence)